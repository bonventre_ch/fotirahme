import tkinter as tk
from PIL import Image, ImageTk, ImageOps
from glob import glob
import os
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler


class ImageHandler(FileSystemEventHandler):
    def __init__(self, window, canvas, label):
        self.image_files = []
        self.window = window
        self.canvas = canvas
        self.label = label
        self.update_image_files()
        self.current_image_index = len(self.image_files) - 1

    def update_image_files(self):
        patterns = ('*.png', '*.jpg', '*.JPG')
        for pattern in patterns:
            files = glob('/home/leroy/Sync/pics/' + pattern)
            self.image_files.extend(files)
        self.image_files.sort(key=os.path.getmtime)

    def on_created(self, event):
        self.update_image_files()
        self.current_image_index = len(self.image_files) - 1

    def change_image(self):
        image_file = self.image_files[self.current_image_index]
        orig_image = Image.open(image_file)
        image = ImageOps.exif_transpose(orig_image)

        max_width = 1920
        max_height = 1080
        image_width, image_height = image.size
        max_ratio = min(max_width / image_width, max_height / image_height)

        new_width = int(image_width * max_ratio)
        new_height = int(image_height * max_ratio)
        image = image.resize((new_width, new_height))

        photo = ImageTk.PhotoImage(image)
        self.label.config(image=photo)
        self.label.image = photo
        self.canvas.create_image((max_width / 2), (max_height / 2), image=photo, anchor='center')
        self.current_image_index = (self.current_image_index - 1) % len(self.image_files)
        self.window.after(3500, self.change_image)  # 5 seconds


def start_slideshow():
    window = tk.Tk()
    window.attributes('-fullscreen', True)
    window.bind('<Escape>', lambda _: window.destroy())

    canvas = tk.Canvas(window, width=window.winfo_screenwidth(), height=window.winfo_screenheight())
    canvas.pack()

    label = tk.Label(window)
    label.pack(fill=tk.BOTH, expand=True)

    image_handler = ImageHandler(window, canvas, label)
    image_handler.on_created(None)
    image_handler.change_image()

    observer = Observer()
    observer.schedule(image_handler, path='/home/leroy/Sync/pics', recursive=False)
    observer.start()

    window.mainloop()


if __name__ == '__main__':
    start_slideshow()
